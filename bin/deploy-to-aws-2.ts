#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib'
import 'source-map-support/register'
import { ArchiversStack } from '../lib/archivers-stack'
import { MonitorsStack } from '../lib/monitors-stack'
import { RegionSetupStack } from '../lib/region-setup-stack'
import { ValidatorsStack } from '../lib/validators-stack'
import { delimiter } from '../utils'

const app = new cdk.App()

const network = app.node.tryGetContext('Network')
const region = app.node.tryGetContext('Region')
const type = app.node.tryGetContext('Type')
const group = app.node.tryGetContext('Group')
const count = app.node.tryGetContext('Count')
const env = app.node.tryGetContext('Env')

switch (type) {
  case 'Setup': {
    if (region) {
      new RegionSetupStack(app, `${region}${delimiter}${type}`, {
        env: { region },
      })
    }
    break
  }

  case 'Monitors': {
    if (region) {
      const regionSetup = new RegionSetupStack(app, `${region}${delimiter}Setup`, {
        env: { region },
      })

      if (network && group && count) {
        new MonitorsStack(app, `${network}${delimiter}${region}${delimiter}${type}${delimiter}${group}`, {
          vpc: regionSetup.vpc,
          securityGroup: regionSetup.securityGroup,
          env: { region },
        })
      }
    }
    break
  }

  case 'Archivers': {
    if (region) {
      const regionSetup = new RegionSetupStack(app, `${region}${delimiter}Setup`, {
        env: { region },
      })

      if (network && group && count) {
        new ArchiversStack(app, `${network}${delimiter}${region}${delimiter}${type}${delimiter}${group}`, {
          vpc: regionSetup.vpc,
          securityGroup: regionSetup.securityGroup,
          env: { region },
        })
      }
    }
    break
  }

  case 'Validators': {
    if (region) {
      const regionSetup = new RegionSetupStack(app, `${region}${delimiter}Setup`, {
        env: { region },
      })

      if (network && group && count) {
        new ValidatorsStack(app, `${network}${delimiter}${region}${delimiter}${type}${delimiter}${group}`, {
          vpc: regionSetup.vpc,
          securityGroup: regionSetup.securityGroup,
          env: { region },
        })
      }
    }
    break
  }
}
