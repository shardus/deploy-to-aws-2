import { CloudFormationClient, ListStacksCommand, StackStatus, StackSummary } from '@aws-sdk/client-cloudformation' // ES Modules import
import { DescribeInstancesCommand, DescribeInstancesCommandOutput, EC2Client, Reservation } from '@aws-sdk/client-ec2'
import { execaCommand } from 'execa'
import { regions } from '../config'

export const delimiter = '--'

enum StackTypes {
  Monitors = 'Monitors',
  Archivers = 'Archivers',
  Validators = 'Validators',
}

interface StackDef {
  network?: string
  region?: string
  type?: StackTypes | 'Setup'
  group?: number
  count?: number
  env?: { [key: string]: string }
}

type Region = {
  name: string
  types: {
    [type in StackTypes]: number
  }
}

interface Network {
  name: string
  regions: {
    [region: string]: Region
  }
}

interface NetworkStacks {
  name: string
  stacks: {
    [type in StackTypes]: StackDef[]
  }
}

interface Change {
  added: StackDef[]
  removed: StackDef[]
  changed: StackDef[]
}

/** Defines an object to hold counts by EC2 region. */
export interface EC2RegionCounts {
  [region: string]: number
}

/**
 * Turns an array of DescribeInstancesCommandOutputs into an EC2RegionCounts
 * object.
 *
 * @param existing
 * @returns EC2RegionCounts
 */
export function reservations2counts(existing: Reservation[]): EC2RegionCounts {
  const counts: EC2RegionCounts = {}

  let region: string | undefined

  for (const reservation of existing) {
    if (!reservation.Instances) throw new Error('No instances in reservation')
    for (const instance of reservation.Instances) {
      region = instance.Placement?.AvailabilityZone?.slice(0, -1)
      if (!region) throw new Error('Region is falsy')
      if (!counts[region]) counts[region] = 1
      else counts[region]++
    }
  }

  return counts
}

/**
 * Uses the given desired number of Validators and Archivers to come up with a
 * Network, that when broken down into NetworkStacks and deployed, would create
 * a network with the given number of Validators and Archivers.
 *
 * @param validators
 * @param archivers
 * @param max
 * @param used
 * @returns
 */
export function desired2network(name: string, validators: number, archivers: number, max: EC2RegionCounts, used: EC2RegionCounts): Network {
  /** Figure out how many EC2 instances are available per region */

  // Get available EC2 resources as an array of [region, instances] sorted by available instances descending
  const available = Object.entries(diffCounts(max, used)).sort((a, b) => {
    if (a[1] < b[1]) return 1
    if (a[1] > b[1]) return -1
    return 0
  })

  /** Spread the desired Validators and Archivers into as many regions as possible*/

  // For each type Monitors, Archivers, then Validators, round robin through regions
  // to allocate into batches
  const desired = {
    Monitors: 1,
    Validators: validators,
    Archivers: archivers,
  }

  const network: Network = {
    name,
    regions: {},
  }

  let type: keyof typeof desired
  for (type in desired) {
    let index = 0
    let retries = 0

    while (desired[type] > 0) {
      const region = available[index][0]
      const remaining = available[index][1]

      // if there's not enough resources in this region
      if (remaining <= 0) {
        retries++
        // if you tried all regions
        if (retries >= available.length) {
          throw new Error(`Not enough resources for ${desired[type]} more ${type}.`)
        }
        // next availability region
        index++
        index = index % available.length
        continue // try again
      }

      // create region in network if it doesn't exist
      if (!network.regions[region]) {
        network.regions[region] = {
          name: region,
          types: {
            Monitors: 0,
            Archivers: 0,
            Validators: 0,
          },
        }
      }

      network.regions[region].types[type]++ // increment network>region>type's instance count
      available[index][1]-- // decrement remaining for this region
      desired[type]-- // decrement desired for this type

      // next availability region
      index++
      index = index % available.length
    }
  }

  return network
}

/**
 * Parse a Network into an EC2RegionCounts object
 *
 * @param network
 * @returns
 */
export function network2counts(network: Network): EC2RegionCounts {
  const counts: EC2RegionCounts = {}

  let region: keyof typeof network.regions
  for (region in network.regions) {
    const total = Object.values(network.regions[region].types).reduce((a, b) => a + b, 0)
    counts[region] = total
  }

  return counts
}

/**
 * Parses a Network into NetworkStacks
 *
 * @param network
 * @param max Max count per stack
 * @returns StackName[]
 */
export function network2stacks(network: Network, max: number): NetworkStacks {
  const stacks: NetworkStacks = {
    name: network.name,
    stacks: {
      Monitors: [],
      Archivers: [],
      Validators: [],
    },
  }

  for (const [region, regionObj] of Object.entries(network.regions)) {
    for (const [type, count] of Object.entries(regionObj.types)) {
      /** Divide count into stack groups of max instances */
      const groups = Math.ceil(count / max)
      const remainder = count % max

      for (let group = 1; group <= groups; group++) {
        const stackDef: StackDef = {
          network: network.name,
          region,
          type: type as StackTypes,
          group,
          count: group === groups ? remainder : max,
        }
        stacks.stacks[type as StackTypes].push(stackDef)
      }
    }
  }

  return stacks
}

/**
 * [TODO] Ensure RegionSetupStacks are deployed for all regions
 */
export async function ensureRegionsAreSetup() {
  // Make a list of regions that don't have a RegionSetupStack
  const regionsToSetup = await Promise.all(
    Object.keys(regions).map(async (region) => {
      const stacks = await getStacks(region)
      if (stacks.find((stack) => stack.StackName?.includes('Setup'))) {
        return undefined
      } else {
        return region
      }
    })
  )

  // Deploy missing RegionSetupStacks
  await Promise.all(
    regionsToSetup.map((region) => {
      if (region) return deployStack({ region, type: 'Setup' })
      else return undefined
    })
  )
}

/**
 * [TODO] Deploy Monitor and Archiver stacks and return their IPs
 *
 * @param NetworkStacks
 * @returns { Monitors: string[], Archivers: string[] }
 */
export function deployMonitorsAndArchivers(NetworkStacks: NetworkStacks): { Monitors: string[]; Archivers: string[] } {
  return { Monitors: [], Archivers: [] }
}

/**
 * [TODO] Deploy Validators and pass them the given Monitor and Archiver IPs
 *
 * @param stacks
 * @param ips
 */
export function deployValidators(stacks: NetworkStacks, ips: { Monitors: string[]; Archivers: string[] }) {}

/**
 * Calculates the difference between two EC2RegionCounts objects as an
 * EC2RegionCounts obj.
 *
 * @param first
 * @param second
 * @returns EC2RegionCounts
 */
export function diffCounts(first: EC2RegionCounts, second: EC2RegionCounts): EC2RegionCounts {
  const result: EC2RegionCounts = {}
  let region: keyof typeof first
  for (region in first) {
    result[region] = first[region] - second[region]
  }
  return result
}

/**
 * Adds two EC2RegionCounts objects together as an EC2RegionCounts object.
 *
 * @param first
 * @param second
 * @returns EC2RegionCounts
 */
export function addCounts(first: EC2RegionCounts, second: EC2RegionCounts): EC2RegionCounts {
  const result: EC2RegionCounts = {}
  let region: keyof typeof first
  for (region in first) {
    result[region] = first[region] + second[region]
  }
  return result
}

export async function getStacks(region: string): Promise<StackSummary[]> {
  let collected = []
  let StackSummaries, NextToken
  do {
    ;({ StackSummaries, NextToken } = await new CloudFormationClient({ region }).send(new ListStacksCommand({ NextToken, StackStatusFilter: [StackStatus.CREATE_COMPLETE, StackStatus.CREATE_IN_PROGRESS] })))
    if (StackSummaries) collected.push(...StackSummaries)
  } while (NextToken)
  return collected
}

export async function getInstances(region: string): Promise<Reservation[]> {
  let collected = []
  let Reservations, NextToken
  do {
    ;({ Reservations, NextToken } = await new EC2Client({ region }).send(new DescribeInstancesCommand({ Filters: [{ Name: 'instance-state-name', Values: ['running'] }] })))
    if (Reservations) collected.push(...Reservations)
  } while (NextToken)
  return collected
}

export async function deployStack(stack: StackDef) {
  let name = ''
  let cmd = 'npx cdk deploy'

  if (stack.network) {
    name += stack.network
    cmd += ' -c Network=' + stack.network
  }
  if (stack.region) {
    name += delimiter + stack.region
    cmd += ' -c Region=' + stack.region
  }
  if (stack.type) {
    name += delimiter + stack.type
    cmd += ' -c Type=' + stack.type
  }
  if (stack.group) {
    name += delimiter + stack.group
    cmd += ' -c Group=' + stack.group
  }

  if (name.startsWith(delimiter)) name = name.slice(delimiter.length)
  cmd += ' ' + name

  console.log('deployStack ', JSON.stringify(stack))
  console.log('deployStack', cmd)
  console.log()

  // const cmdProc = await execaCommand(cmd, { all: true })
  const cmdProc = Promise.resolve(cmd)

  return cmdProc
}
