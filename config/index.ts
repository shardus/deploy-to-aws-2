import { EC2RegionCounts } from '../utils'

export const regions: EC2RegionCounts = {
  'us-east-1': 5016,
  //'af-south-1', // 856 //we had problems with this region in recent testing
  'ap-east-1': 856,
  'eu-central-1': 1112,
  'eu-north-1': 896,
  'eu-south-1': 856,
  'eu-west-1': 3000,
  'eu-west-3': 890,
  'sa-east-1': 3000,
  'us-west-2': 1112,
}

export const maxPerStack = 100

export const maxSimultaneousDeploy = 5
