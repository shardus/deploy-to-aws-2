import execa = require('execa')
import DraftLog = require('draftlog')
import { maxPerStack, regions } from '../config'
import { deployMonitorsAndArchivers, deployValidators, desired2network, ensureRegionsAreSetup, getInstances, network2stacks, reservations2counts } from '../utils'

DraftLog(console).addLineListener(process.stdin)

/**
 * script deploy <NETWORK_NAME> <VALIDATOR_COUNT> [ARCHIVER_COUNT]
 */
async function main() {
  const name = process.argv[2] || ''
  const validators = Number(process.argv[3]) || 0
  const archivers = Number(process.argv[4]) || 0
  // const existingArchivers = process.argv[5] || '[]'
  // const existingMonitorUrl = process.argv[6] || ''

  /** Figure out existing EC2 instance usage */

  const used = reservations2counts((await Promise.all(Object.keys(regions).map((region) => getInstances(region)))).flat())

  /** Use the given parameters to create a Network */

  const network = desired2network(name, validators, archivers, regions, used)

  /** Parse the Network into NetworkStacks */

  const stacks = network2stacks(network, maxPerStack)

  /** Deploy the NetworkStacks to AWS */

  // Ensure RegionSetupStacks are deployed
  await ensureRegionsAreSetup()

  /*
  // Deploy Monitors and Archivers and save IPs
  const ips = deployMonitorsAndArchivers(stacks)

  // Deploy Validators and give them Monitor and Archiver IPs
  deployValidators(stacks, ips)
  */

  /** Exit the process cleanly */

  // Without this it hangs for some reason
  process.exit()
}

main()

function getNewestLine(label: string, chunk: Buffer | string, buffer: string): string {
  // Save chunk to buffer
  buffer += chunk.toString()
  // Walk backwards through buffer until newline char
  let newLine = []
  let char
  const start = buffer.length - 1
  for (let i = start; i > -1; i--) {
    char = buffer[i]
    if (char === '\n' && i !== start) break
    else newLine.push(char)
  }
  // Return newest line
  const newestLine = label + newLine.reverse().join('')
  const maxLen = process.stdout.columns || 100
  if (newestLine.length > maxLen) {
    return newestLine.substr(0, maxLen - 5) + '...'
  } else {
    return newestLine
  }
}
