import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib'
import { InstanceType, Port, SecurityGroup, SubnetType, Vpc } from 'aws-cdk-lib/aws-ec2'
import { EcsOptimizedImage } from 'aws-cdk-lib/aws-ecs'
import { Construct } from 'constructs'
import { config } from 'dotenv'
import { resolve } from 'path'
import { DockerInstance } from './docker-instance'

config({ path: resolve(__dirname, '../.secrets') })

interface ValidatorsStackProps extends StackProps {
  vpc?: Vpc
  securityGroup?: SecurityGroup
}

export class ValidatorsStack extends Stack {
  constructor(scope: Construct, id: string, props: ValidatorsStackProps) {
    super(scope, id, props)

    const vpc = props.vpc
      ? props.vpc
      : new Vpc(this, 'VPC', {
          maxAzs: 1,
          subnetConfiguration: [
            {
              name: 'Ingress',
              subnetType: SubnetType.PUBLIC,
            },
          ],
        })

    const securityGroup = props.securityGroup
      ? props.securityGroup
      : new SecurityGroup(this, 'SecruityGroup', {
          vpc,
        })
    securityGroup.connections.allowFromAnyIpv4(Port.allTraffic())

    const environment: any = {}
    environment[this.node.tryGetContext('AppSeedlistEnvVar')] = `${this.node.tryGetContext('ArchiverIp')}`
    environment[this.node.tryGetContext('AppMonitorEnvVar')] = `${this.node.tryGetContext('MonitorIp')}`
    environment[this.node.tryGetContext('AppIpAddrEnvVar')] = '`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`'
    environment['NODE_OPTIONS'] = '--max-old-space-size=2000' //test 800mb for t3 micro/small.   medium=3584 IMPORTANT; set this to slightly less than the EC2 instance types memory

    for (let i = 0; i < this.node.tryGetContext('Count'); i++) {
      const validator = new DockerInstance(this, `NetworkNode${i}`, {
        imageUri: this.node.tryGetContext('AppImageUri'),
        username: process.env.AppRegistryUsername,
        token: process.env.AppRegistryToken,
        environment,
        instanceType: new InstanceType('t3.medium'),
        machineImage: EcsOptimizedImage.amazonLinux2(),
        vpc,
        instanceName: 'Validator',
        keyName: 'shardus-ent-cf',
        securityGroup,
        vpcSubnets: {
          subnetType: SubnetType.PUBLIC,
        },
      })

      new CfnOutput(this, `ValidatorIP-${i}`, {
        value: validator.instancePublicIp,
      })
    }
  }
}
