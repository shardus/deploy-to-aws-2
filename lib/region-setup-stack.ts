import { Stack, StackProps } from 'aws-cdk-lib'
import { Port, SecurityGroup, SubnetType, Vpc } from 'aws-cdk-lib/aws-ec2'
import { Construct } from 'constructs'
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export interface RegionSetupStack extends Stack {
  vpc: Vpc
  securityGroup: SecurityGroup
}

export class RegionSetupStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props)

    // Create a vpc
    this.vpc = new Vpc(this, 'VPC', {
      maxAzs: 1,
      subnetConfiguration: [
        {
          name: 'Ingress',
          subnetType: SubnetType.PUBLIC,
        },
      ],
    })

    // Create a security group
    this.securityGroup = new SecurityGroup(this, 'SecruityGroup', {
      vpc: this.vpc,
    })
    this.securityGroup.connections.allowFromAnyIpv4(Port.allTraffic())
  }
}
