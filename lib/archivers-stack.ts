import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib'
import { InstanceType, Port, SecurityGroup, SubnetType, Vpc } from 'aws-cdk-lib/aws-ec2'
import { EcsOptimizedImage } from 'aws-cdk-lib/aws-ecs'
import { Construct } from 'constructs'
import { config } from 'dotenv'
import { resolve } from 'path'
import { DockerInstance } from './docker-instance'

config({ path: resolve(__dirname, '../.secrets') })

interface ArchiversStackProps extends StackProps {
  vpc?: Vpc
  securityGroup?: SecurityGroup
}

export class ArchiversStack extends Stack {
  constructor(scope: Construct, id: string, props: ArchiversStackProps) {
    super(scope, id, props)

    const vpc = props.vpc
      ? props.vpc
      : new Vpc(this, 'VPC', {
          maxAzs: 1,
          subnetConfiguration: [
            {
              name: 'Ingress',
              subnetType: SubnetType.PUBLIC,
            },
          ],
        })

    const securityGroup = props.securityGroup
      ? props.securityGroup
      : new SecurityGroup(this, 'SecruityGroup', {
          vpc,
        })
    securityGroup.connections.allowFromAnyIpv4(Port.allTraffic())

    // Get ExistingArchivers
    const existingArchivers: string = this.node.tryGetContext('ExistingArchivers')

    const environment: any = {}
    environment[this.node.tryGetContext('ArchiverIpAddrEnvVar')] = '`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`'
    if (existingArchivers && existingArchivers !== '') {
      environment['ARCHIVER_EXISTING'] = existingArchivers
    }

    for (let i = 0; i < this.node.tryGetContext('Count'); i++) {
      const archiver = new DockerInstance(this, `Archiver${i}`, {
        imageUri: this.node.tryGetContext('ArchiverImageUri'),
        username: process.env.ArchiverRegistryUsername,
        token: process.env.ArchiverRegistryToken,
        environment,
        instanceType: new InstanceType('t3.large'),
        machineImage: EcsOptimizedImage.amazonLinux2(),
        vpc,
        instanceName: 'Archiver',
        keyName: 'shardus-ent-cf',
        securityGroup,
        vpcSubnets: {
          subnetType: SubnetType.PUBLIC,
        },
      })

      new CfnOutput(this, `ArchiverIP-${i}`, {
        value: archiver.instancePublicIp,
      })
    }
  }
}
