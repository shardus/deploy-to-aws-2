import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib'
import { InstanceType, Port, SecurityGroup, SubnetType, Vpc } from 'aws-cdk-lib/aws-ec2'
import { EcsOptimizedImage } from 'aws-cdk-lib/aws-ecs'
import { Construct } from 'constructs'
import { DockerInstance } from './docker-instance'
// import * as sqs from 'aws-cdk-lib/aws-sqs';

interface MonitorStackProps extends StackProps {
  vpc?: Vpc
  securityGroup?: SecurityGroup
}

export class MonitorsStack extends Stack {
  constructor(scope: Construct, id: string, props: MonitorStackProps) {
    super(scope, id, props)

    const vpc = props.vpc
      ? props.vpc
      : new Vpc(this, 'VPC', {
          maxAzs: 1,
          subnetConfiguration: [
            {
              name: 'Ingress',
              subnetType: SubnetType.PUBLIC,
            },
          ],
        })

    const securityGroup = props.securityGroup
      ? props.securityGroup
      : new SecurityGroup(this, 'SecruityGroup', {
          vpc,
        })
    securityGroup.connections.allowFromAnyIpv4(Port.allTraffic())

    const monitor = new DockerInstance(this, 'Monitor', {
      imageUri: this.node.tryGetContext('MonitorImageUri'),
      instanceType: new InstanceType('t3.micro'),
      machineImage: EcsOptimizedImage.amazonLinux2(),
      vpc,
      instanceName: 'Monitor',
      keyName: 'shardus-ent-cf',
      securityGroup,
      vpcSubnets: {
        subnetType: SubnetType.PUBLIC,
      },
    })

    new CfnOutput(this, 'MonitorIp', {
      value: monitor.instancePublicIp,
    })
  }
}
